#!/usr/bin/env bash

{
printf "PID\tUID\tPPID\tNAME\tSTATE\tstart_time\tvsize\trss\tutime\tstime\n"

# print all proceses
for i in $(ls -l /proc/ | grep -v "self" | egrep "[0-9]$" | awk '{ print $9 }');
do
        pid=$i
        user=$(         stat -c %U /proc/$i )
        ppid=$(         awk '{print $4}' /proc/$i/stat )
        name=$(         awk '{print $2}' /proc/$i/stat )
        state=$(        awk '{print $3}' /proc/$i/stat )

        #ms after start
        start_time=$(   awk '{print $22}' /proc/$i/stat)

        #mem
        vsize=$(        awk '{print $23}' /proc/$i/stat)
        rss=$(          awk '{print $24}' /proc/$i/stat)

        #usertime
        utime=$(        awk '{print $14}' /proc/$i/stat)
        #system time
        stime=$(        awk '{print $15}' /proc/$i/stat)

        printf "$pid\t"
        printf "$user\t"
        printf "$ppid\t"
        printf "$name\t"
        printf "$state\t"
        printf "$start_time\t"
        printf "$vsize\t"
        printf "$rss\t"
        printf "$utime\t"
        printf "$stime\t"

        printf "\n"
done } 2> /dev/null | column -t
