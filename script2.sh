#!/usr/bin/env bash

{
file_name=$1

if [[ $# -eq 0 ]];
then

        echo "need full filename"
        echo "example: ./script2.sh /var/log/messages"
        exit 1
fi


printf "USER\t\tNAME\t\tPID\tCMD\n"
for i in $(ls -l /proc/ | grep -v "self" | egrep "[0-9]$" | awk '{ print $9 }');
do
        file=$( stat -c %N  /proc/$i/fd/* | grep $file_name )
        if [[ $? -eq 0 ]];
        then
                user=$( stat -c %U /proc/$i )
                name=$( awk '{print $2}' /proc/$i/stat )
                pid=$i
                cmd=$(cat /proc/$i/cmdline)

                printf "$user\t$name\t$pid\t$cmd\n"
        fi
done } 2> /dev/null | column -t

